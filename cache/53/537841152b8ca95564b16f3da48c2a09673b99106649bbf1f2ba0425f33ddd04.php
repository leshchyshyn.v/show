<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* Product.html */
class __TwigTemplate_50f1a233e5e26c748aaedd0f8633477dca17e5c0f4a2e7961decb568d0206827 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
    <title>Product page</title>
</head>
<body>
<h1>Product</h1>
<ul>
    ";
        // line 9
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "id", [], "any", false, false, false, 9), "html", null, true);
        echo "
    <br/>
    ";
        // line 11
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "name", [], "any", false, false, false, 11), "html", null, true);
        echo "
    <br/>
    ";
        // line 13
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "description", [], "any", false, false, false, 13), "html", null, true);
        echo "
    <br/>


    <form method=\"post\" action=\"/product/remove\">
        <input type=\"hidden\" name=\"id\" value=\"";
        // line 18
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "id", [], "any", false, false, false, 18), "html", null, true);
        echo "\" />
        <button type=\"submit\">Remove</button>
    </form>
</ul>

</body>
</html>";
    }

    public function getTemplateName()
    {
        return "Product.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  65 => 18,  57 => 13,  52 => 11,  47 => 9,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html>
<head>
    <title>Product page</title>
</head>
<body>
<h1>Product</h1>
<ul>
    {{data.id}}
    <br/>
    {{data.name}}
    <br/>
    {{data.description}}
    <br/>


    <form method=\"post\" action=\"/product/remove\">
        <input type=\"hidden\" name=\"id\" value=\"{{data.id}}\" />
        <button type=\"submit\">Remove</button>
    </form>
</ul>

</body>
</html>", "Product.html", "/Users/volodymyrleshchyshyn/Sites/shop/Shop/View/Product.html");
    }
}
