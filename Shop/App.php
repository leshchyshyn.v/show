<?php
namespace Shop;

use Shop\Service\Router;

class App
{
    public static function run()
    {
        $router = new Router();
        $router->dispatch();
    }
}