<?php

namespace Shop\Service;

use \Klein\Klein;
use \Shop\Controller\ControllerInterface;

class Router
{
    /**
     * @var array
     */
    public $routes = [
        [
            'method' => "GET",
            'path' => "/",
            'className' => \Shop\Controller\Home::class
        ],

        [
            'method' => "GET",
            'path' => "/migrate/",
            'className' => \Shop\Controller\Database\MigrateController::class
        ],

        [
            'method' => "GET",
            'path' => "/product/view/[i:id]",
            'className' => \Shop\Controller\Product\ViewController::class
        ],

        [
            'method' => "GET",
            'path' => "/product/list/",
            'className' => \Shop\Controller\Product\ListController::class
        ],

        [
            'method' => "GET",
            'path' => "/product/new",
            'className' => \Shop\Controller\Product\NewController::class
        ],

        [
            'method' => "POST",
            'path' => "/product/save",
            'className' => \Shop\Controller\Product\SaveController::class
        ],

        [
            'method' => "GET",
            'path' => "/product/edit/[i:id]",
            'className' => \Shop\Controller\Product\EditController::class
        ],
        [
            'method' => "POST",
            'path' => "/product/remove",
            'className' => \Shop\Controller\Product\RemoveController::class
        ],
    ];


    public function dispatch()
    {
        $klein = new Klein();

        foreach ($this->routes as $route) {
            $klein->respond(
                $route['method'],
                $route['path'],
                function ($request, $response) use ($route) {
                    $controller = DiContainer::getInstance()->get($route['className']);
//                    $controller = new $route['className']();

                    if ($controller instanceof ControllerInterface) {

                        return $controller->execute($request, $response);

                    } else {

                        throw new SystemException('Controller Class not found');

                    }
                }
            );
        }

        $klein->dispatch();
    }
}