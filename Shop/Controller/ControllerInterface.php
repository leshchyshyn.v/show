<?php

namespace Shop\Controller;

interface ControllerInterface
{
    public function execute($request, $response);
}