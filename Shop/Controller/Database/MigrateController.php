<?php

namespace Shop\Controller\Database;

use Shop\Controller\ControllerInterface;
use Shop\Service\Database;

class MigrateController implements ControllerInterface
{
    public function execute($request, $response)
    {

        try {
            $em = Database::GetEntityManager();
            $tool = new \Doctrine\ORM\Tools\SchemaTool($em);
            $classes = array(
                $em->getClassMetadata('\Shop\Model\Product'),
                $em->getClassMetadata('\Shop\Model\Order'),
            );
            $tool->createSchema($classes);
            return 'Schema created succesfully!';
        }
        catch (\Exception $e)
        {
            return $e->getMessage();

        }



    }
}
