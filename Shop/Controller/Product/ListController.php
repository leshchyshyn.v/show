<?php
namespace  Shop\Controller\Product;

use Shop\Controller\ControllerInterface;
use Shop\Service\Database;
use Shop\Model\Product;
use Shop\Service\NotFoundException;
use Twig\Environment;

class ListController implements ControllerInterface
{
    private $product;
    private $twig;
    public function __construct(
        Product $product,
        Environment $twig
    )
    {
        $this->product = $product;
        $this->twig = $twig;
    }

    public function execute($request, $response)
    {
        try {
            $this->product = Database::GetEntityManager()
                ->getRepository(Product::class)
                ->findAll();

            if(!$this->product)
            {
                throw new NotFoundException();
            }

            var_dump($this->product);die;

            $renderParams = array('data'=>$this->product);

            $template = $this->twig->load('Product.html');
            return $template->render($renderParams);

        } catch (NotFoundException $e) {
            $template = $this->twig->load('noresults.html');
            return  $template->render('noresults.html');

        } catch (\Exception $exception) {
            echo  $exception->getMessage();
        }
    }
}