<?php

namespace Shop\Controller\Product;

use Shop\Controller\ControllerInterface;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class NewController implements ControllerInterface
{
    public function execute($request, $response)
    {
        $loader = new FilesystemLoader(DOCKROOT.'/Shop/View');
        $template = new Environment($loader);
        return  $template->render('newproduct.html');
    }
}