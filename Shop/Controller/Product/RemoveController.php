<?php

namespace Shop\Controller\Product;

use Shop\Controller\ControllerInterface;
use Shop\Service\Database;
use Shop\Model\Product;
use Shop\Service\NotFoundException;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class RemoveController implements ControllerInterface
{
    public function execute($request, $response)
    {
        try {
            $params = $request->paramsPost();
            $product = Database::GetEntityManager()
                ->getRepository(Product::class)
                ->find($params['id']);

            if(!$product)
            {
                throw new NotFoundException();
            }

            Database::GetEntityManager()->remove($product);
            Database::GetEntityManager()->flush();
            return $response->redirect("/");

        } catch (NotFoundException $e) {
            $loader = new FilesystemLoader(DOCKROOT.'/Shop/View');
            $template = new Environment($loader);
            return  $template->render('noresults.html');

        } catch (\Exception $exception) {
            echo  $exception->getMessage();
        }
    }
}