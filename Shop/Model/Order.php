<?php
namespace Shop\Model;

/**
 * Class Order
 * @package Shop\Model
 * @Entity @Table(name="order")
 */
class Order
{
    /**
     * @var integer
     * @Id @Column(type="integer") @GeneratedValue
     */
    private $id;

    /**
     * @var string
     * @Column(type="string")
     */
    private $email;

    /**
     * @var string
     * @Column(type="float")
     */
    private $total;

    /**
     * @ORM\OneToMany(targetEntity="Shop\Model\Product", inversedBy="order")
     */
    private $products;

    /**
     * Order constructor.
     */
    public function __construct()
    {
        $this->products = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    /**
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param string $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    public function getData()
    {
        return $this->getName();
    }
}